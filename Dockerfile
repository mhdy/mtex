FROM debian:bullseye

RUN apt-get update \
  && apt-get install -y texlive-full diffpdf pandoc \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir /shims
COPY shims/*.sh /shims/

RUN mkdir -p /usr/local/share/texmf/tex/latex/
COPY classes/* /usr/local/share/texmf/tex/latex/
RUN texhash

RUN mkdir /workspace
WORKDIR /workspace

CMD ["/bin/bash"]
