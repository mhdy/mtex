PREFIX := ~/.local
CONFIG := ~/.config
DOCKER_IMAGE := mhdy/mtex:latest

build:
	docker build -t $(DOCKER_IMAGE) .
	docker image prune -f

install:
	mkdir -p $(PREFIX)/bin/
	install -m 700 mtex $(PREFIX)/bin/
	mkdir -p $(CONFIG)/mtex/
	cp -f mtex.conf $(CONFIG)/mtex/
	mkdir -p $(PREFIX)/share/mtex/
	cp -r templates/ $(PREFIX)/share/mtex/

uninstall:
	rm -rf $(PREFIX)/bin/mtex $(PREFIX)/share/mtex $(CONFIG)/mtex
	@echo "----------------------------------------------------"
	@echo "The mtex docker image should be removed manually"
	@echo "by executing: docker image rm $(DOCKER_IMAGE)"
	@echo "----------------------------------------------------"
