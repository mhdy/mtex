# mtex

mtex is a POSIX shell script that helps compile latex sources.
It uses a docker image consisting of debian with the texlive-full package and some scripts for compiling latex sources. 
This tool helps keeping the host system clean and reducing the overhead of installing latex utilities.
mtex also comes with a templates system which helps initialize and provision latex projects.
I use this as part of my scientific research workflow.

## Installation

Assuming [docker is installed](https://docs.docker.com/install/) in the host system, execute the following:

Clone this git repository:
```
git clone https://gitlab.com/mhdy/mtex.git
cd mtex
make install
```

Then, build the docker image as follows.
The build may take a while since it uses the texlive-full package which is big.
```
make build
```

## Usage

```
mtex t|templates
  List available templates.
mtex i|init|initialize [-f] TEMPLATE
  Initialize the current directory with the specified template.
  Option -f forces initialization and overwrites existing files.
mtex g|gen|generate FORMAT [MAIN_FILE]
  Compile latex source to generate the specified format.
  FORMAT is one of the following: pdf, dvi, ps or all.
  MAIN_FILE is the relative path to the main file. Default: main.tex
mtex c|compile [MAIN_FILE]
  This is an alias of `mtex g pdf'.
mtex r|run CMD
  Run CMD inside mtex container while mounting the current working directory.
mtex s|shell
  Spawn a docker container mounting the current working directory.
mtex h|help
  Display this help and exit.
```

### How to

#### How to use mtex with vim?

Append the following lines to the .vimrc file:
```
command -nargs=0 Mtex !mtex c 
ab mtex Mtex
```

Now, you can compile latex code within vim using the command `:mtex`

#### How to build a custom docker image?

1. Edit the Dockerfile according to your needs.
2. Change the name of your image in the `Makefile`.
3. Similarly, change the directive `docker_image` in the configuration file `~/.config/mtex/mtex.conf`.
4. Execute `make build`.

Note that name of the custom docker image must be the same in the `Makefile` and the configuration file `~/.config/mtex/mtex.conf`.

#### How to add templates?

Place custom templates inside the templates directory.
It defaults to `~/.local/share/mtex/templates/`.

#### How to add custom document classes or custom themes?

Place related files inside the classes directory.
It defaults to `./classes/`, then rebuild the mtex image using `make build`.

#### How to change templates directory?

Change the directive `templates_path` 
in the configuration file `~/.config/mtex/mtex.conf`.

## License

MIT.
