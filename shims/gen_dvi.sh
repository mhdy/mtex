#!/bin/sh

PROGRAM_NAME="$(basename $0)"

EXIT_SUCCESS=0
EXIT_FAILURE=1

fail() {
  echo "mtex: $PROGRAM_NAME: $1" >&2
  exit $EXIT_FAILURE
}

###
### main
###

main_file_path="$(realpath --relative-to=/workspace "$1" 2> /dev/null)"
[ $? -ne 0 ] && fail "cannot find \`$1': expecting relative path"

latexmk \
  -dvi \
  -interaction=nonstopmode \
  -synctex=-1 \
  "/workspace/$main_file_path" > /tmp/out.txt 2>&1

n="$(grep -n 'Run number' /tmp/out.txt | tail -1 | cut -d ':' -f 1)"
[ -z "$n" ] && cat /tmp/out.txt || tail -n +$((n + 2)) /tmp/out.txt

rm -f /tmp/out.txt

exit $EXIT_SUCCESS
