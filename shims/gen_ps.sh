#!/bin/sh

PROGRAM_NAME="$(basename $0)"

EXIT_SUCCESS=0
EXIT_FAILURE=1

fail() {
  echo "mtex: $PROGRAM_NAME: $1" >&2
  exit $EXIT_FAILURE
}

gen_ps() {
  main_file_path="$1"
  main_dir="$(dirname "$main_file_path")"
  main_file="$(basename "$main_file_path" '.tex')"
  [ -f "$main_dir/$main_file.pdf" ] && pdfexists=1 || pdfexists=0
  /shims/gen_pdf.sh "$main_file_path"
  pdf2ps "$main_dir/$main_file.pdf" "$main_dir/$main_file.ps"
  [ $pdfexists -eq 0 ] && rm -f "$main_dir/$main_file.pdf"
}

###
### main
###

main_file_path="$(realpath --relative-to=/workspace "$1" 2> /dev/null)"
[ $? -ne 0 ] \
  && fail "cannot find \`$1': expecting relative path"

gen_ps "/workspace/$main_file_path"

exit $EXIT_SUCCESS
