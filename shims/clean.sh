#!/bin/sh

PROGRAM_NAME="$(basename $0)"

EXIT_SUCCESS=0
EXIT_FAILURE=1

clean() {
  latexmk -c > /dev/null 2>&1
  rm -rf *.bbl *.blg *.aux *.synctex *.ptc *.snm *.nav *.vrb
}

clean

exit $EXIT_SUCCESS
